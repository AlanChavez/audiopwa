const cacheName = 'my-pwa-cache-v1';
const cacheFiles = [
    '/',
    '/index.html',
    '/style.css',
    '/script.js',
    '/icon.png',
    '/manifest.js'
];

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(cacheName).then(function (cache) {
            return cache.addAll(cacheFiles);
        })
    );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});
