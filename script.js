const startRecordButton = document.getElementById("startRecord");
const stopRecordButton = document.getElementById("stopRecord");
const playAudioButton = document.getElementById("playAudio");
const audioPlayer = document.getElementById("audioPlayer");

let mediaRecorder;
let audioChunks = [];
let isRecording = false; // Variable para rastrear el estado de grabación

function updateButtonStyles() {
    // Cambiar las clases de los botones según el estado de grabación
    if (isRecording) {
        startRecordButton.classList.remove("recording-button");
        startRecordButton.classList.add("not-recording-button");
        stopRecordButton.classList.remove("not-recording-button");
        stopRecordButton.classList.add("recording-button");
    } else {
        startRecordButton.classList.remove("not-recording-button");
        startRecordButton.classList.add("recording-button");
        stopRecordButton.classList.remove("recording-button");
        stopRecordButton.classList.add("not-recording-button");
    }
}

startRecordButton.addEventListener("click", async () => {
    try {
        if (!isRecording) {
            const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

            mediaRecorder = new MediaRecorder(stream);

            mediaRecorder.ondataavailable = event => {
                if (event.data.size > 0) {
                    audioChunks.push(event.data);
                }
            };

            mediaRecorder.onstop = () => {
                try {
                    const audioBlob = new Blob(audioChunks, { type: 'audio/wav' });
                    audioPlayer.src = URL.createObjectURL(audioBlob);
                } catch (error) {
                    console.error('Error al crear el Blob de audio:', error);
                }
            };

            mediaRecorder.start();
            isRecording = true;
            updateButtonStyles(); // Actualizar los estilos de los botones
        } else {
            mediaRecorder.stop();
            isRecording = false;
            updateButtonStyles(); // Actualizar los estilos de los botones
        }
    } catch (error) {
        console.error('Error al acceder al dispositivo de grabación de audio:', error);
    }
});

playAudioButton.addEventListener("click", () => {
    audioPlayer.play();
});

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js')
        .then(function (registration) {
            console.log('Service Worker registrado con éxito:', registration);
        })
        .catch(function (error) {
            console.error('Error al registrar el Service Worker:', error);
        });
}
